using System;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using LootLocker.Requests;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class LeaderboardController : MonoBehaviour
{
    //Create a singelton object
    public static LeaderboardController instance;
    int memberIDInt; 
    string memberID;
    public int leaderboardID = 123;
    public bool bEnableLootLocker = true;
    public TextMeshProUGUI[] Entries;
    public TextMeshProUGUI PlayerRank;
    public TextMeshProUGUI PlayerScore;
    public GameObject LeaderboardCanvas;
    public string PlayerName = "fuck";
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }
    
    // Start is called before the first frame update
    void Start()
    {
        memberID = RuntimePlatform.WebGLPlayer.GetHashCode().ToString();
        if (bEnableLootLocker)
        {
            //memberIDInt = Random.Range(0, Int32.MaxValue);
            memberID = memberIDInt.ToString();
            StartLootLockerSession();
            SetPlayerName(PlayerName);
        }
    }

    void StartLootLockerSession()
    {
        LootLockerSDKManager.StartGuestSession((response) =>
        {
            if (!response.success)
            {
                Debug.Log("error starting LootLocker session");

                return;
            }
            Debug.Log("successfully started LootLocker session");
            Debug.Log(memberID);
            Debug.Log(response.player_id);
        });
    }

    void SetPlayerName(string playerName)
    {
        LootLockerSDKManager.SetPlayerName(playerName, (response) =>
        {
            if (response.statusCode == 200) {
                Debug.Log("Successful name set" + playerName);
            } else {
                Debug.Log("failed: " + response.Error);
            }
        });
    }
    public void ReportToLeaderboard(int score)
    {
        LootLockerSDKManager.SubmitScore(memberID, score, leaderboardID, (response) =>
        {
            if (response.statusCode == 200) {
                Debug.Log("Successful submit score");
            } else {
                Debug.Log("failed: " + response.Error);
            }
        });
    }

    public void GetPlayerScore(int playerID)
    {
        LootLockerSDKManager.GetMemberRank(leaderboardID.ToString(), playerID, (response) =>
        {
            if (response.statusCode == 200)
            {
                PlayerRank.text = response.rank.ToString();
                PlayerScore.text = response.score.ToString();
            } else {
                Debug.Log("failed: " + response.Error);
            }
        });
    }

    public void GetTopScores()
    {
        int count = 10;
        LeaderboardCanvas.SetActive(true);
        LootLockerSDKManager.GetScoreList(leaderboardID, count,(response) =>
        {
            if (response.statusCode == 200)
            {
                GetPlayerScore(memberIDInt);
                LootLockerLeaderboardMember[] scores = response.items;

                for (int i = 0; i < scores.Length; i++)
                {
                    Debug.Log(scores[i].rank + " " + scores[i].score);
                    Entries[i].text = scores[i].score.ToString();
                }
            } else {
                Debug.Log("failed: " + response.Error);
            }
        });
    }
// Update is called once per frame
    void Update()
    {
        
    }
}
